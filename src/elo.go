package main

import (
    "fmt"
    "math"
)

// Constants
const W, D, L string = "W", "D", "L"

func elo(p1score float64, p2score float64, result string) (float64, float64) {
    // Constants
    sigmoidFactor := 50.0
    k           := 10.0
    wWin        := 1.0
    wDraw       := 0.5
    wLoose      := 0.0
    drawShift   := 1.0
    scoreDiff   := - (p1score - p2score)
    p1luck      := (1.0 / (1.0 + math.Exp(scoreDiff / sigmoidFactor)))
    p2luck      := 1 - p1luck
    if (result == W) {
        p1score += (k * (wWin - p1luck))
        p2score += (k * (wLoose - p2luck))
        fmt.Printf("%03.6f - %03.6f\n", p1luck, p2luck)
    }
    if (result == D) {
        p1score += ((k * (wDraw - p1luck)) + drawShift)
        p2score += ((k * (wDraw - p2luck)) + drawShift)
        fmt.Printf("%03.6f - %03.6f\n", p1luck, p2luck)
    }
    if (result == L) {
        p1score += (k * (wLoose - p1luck))
        p2score += (k * (wWin - p2luck))
        fmt.Printf("%03.6f - %03.6f\n", p1luck, p2luck)
    }
    return p1score, p2score
}

func main() {
    fmt.Printf("Exp function for 42 is %g.\n", math.Exp(42))
    p1score, p2score := elo(100, 100, W)
    fmt.Printf("Calculate ELO if 100 / 100 and W: %03.6f and %03.6f\n", p1score, p2score)
}
